import React from "react";
import {drawEditor, useForceUpdate} from "./util";

function findPos(obj) {
  let curleft = 0;
  let curtop = 0;
  if (obj.offsetParent) {
    do {
      curleft += obj.offsetLeft;
      curtop += obj.offsetTop;
    } while ((obj = obj.offsetParent));
  }
  return {
    left: curleft,
    top: curtop,
  };
}

export default React.memo(function Editor({trackRef, type, index}) {
  let canvasRef = React.useRef();
  let forceUpdate = useForceUpdate();
  let height = type === "drum" ? 240 : 720;

  React.useEffect(() => {
    drawEditor(canvasRef.current, height, type, trackRef.current[index].steps);
  });

  function handleClick(e) {
    let {left, top} = findPos(canvasRef.current);
    let x = e.pageX - left;
    let y = e.pageY - top;
    let steps = trackRef.current[index].steps;
    if (x > 24) {
      let realX = x - 24;
      let step = Math.floor(realX / (656 / 32));
      let note = Math.floor(y / 20);
      let value = steps[step][note];
      steps[step][note] = value === null ? true : null;
      forceUpdate();
    }
  }

  return (
    <canvas
      className="editor"
      ref={canvasRef}
      width={680}
      height={height}
      onClick={handleClick}
    />
  );
});
