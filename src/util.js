import { useState } from "react";

export function useForceUpdate() {
  const [value, setValue] = useState(0);
  return () => setValue((value) => ++value);
}

export const createDrumSteps = () => {
  let steps = [];
  for (let i = 0; i < 32; ++i) {
    let step = [];
    for (let j = 0; j < 9; ++j) {
      step.push(null);
    }
    steps.push(step);
  }
  return steps;
};

export const createNoteSteps = () => {
  let steps = [];
  for (let i = 0; i < 32; ++i) {
    let step = [];
    for (let j = 0; j < 24; ++j) {
      step.push(null);
    }
    steps.push(step);
  }
  return steps;
};

const labelMap = ["kick", "snar", "chat", "ohat", "clap", "rim", "crsh", "ltom", "htom", "mtm1", "mtm2", "ftom"];

// visual representation of piano keys for easier mapping of keys
//  1 3    6 8 10    13 15    18 20 22     25 27   30 32 34
// 0 2 4  5 7 9 11  12 14 16 17 19 21 23 24 26 28 29 31 33 35
const blackKeys = [1, 3, 6, 8, 10, 13, 15, 18, 20, 22, 25, 27, 30, 32, 34];

export const drawEditor = (canvas, height, type, steps) => {
  const WIDTH = 680;
  const HEIGHT = height;
  const SIDE_WIDTH = 24;
  const TOTAL_WIDTH = 680;
  const EDITOR_WIDTH = TOTAL_WIDTH - SIDE_WIDTH;
  const STEP_WIDTH = EDITOR_WIDTH / 32;
  const STEP_HEIGHT = 20;

  if (canvas) {
    let ctx = canvas.getContext("2d");

    ctx.fillStyle = "#222";
    ctx.clearRect(0, 0, WIDTH, HEIGHT);
    ctx.fillRect(0, 0, WIDTH, HEIGHT);

    ctx.fillStyle = "#333";
    ctx.fillRect(SIDE_WIDTH + STEP_WIDTH * 4, 0, STEP_WIDTH * 4, height);
    ctx.fillRect(SIDE_WIDTH + STEP_WIDTH * 12, 0, STEP_WIDTH * 4, height);
    ctx.fillRect(SIDE_WIDTH + STEP_WIDTH * 20, 0, STEP_WIDTH * 4, height);
    ctx.fillRect(SIDE_WIDTH + STEP_WIDTH * 28, 0, STEP_WIDTH * 4, height);

    if (type === "drum") {
      for (let i = 0; i < labelMap.length; ++i) {
        ctx.fillStyle = "#444";
        ctx.fillRect(0, STEP_HEIGHT * i, SIDE_WIDTH, STEP_HEIGHT);
        ctx.fillStyle = "white";
        ctx.textBaseline = "top";
        ctx.textAlign = "center";
        ctx.font = "10px monospace";
        ctx.fillText(labelMap[i], 12, STEP_HEIGHT * i + labelMap.length);
        console.log(labelMap[i]);
        if (i !== 0) {
          ctx.lineWidth = 0;
          ctx.strokeStyle = "#666";
          ctx.moveTo(0, STEP_HEIGHT * i + 0.5);
          ctx.lineTo(TOTAL_WIDTH, STEP_HEIGHT * i + 0.5);
          ctx.stroke();
        }
      }
      ctx.lineWidth = 0;
      ctx.strokeStyle = "#666";
      // draw
      for (let i = 0; i < 32; ++i) {
        ctx.moveTo(i * STEP_WIDTH + SIDE_WIDTH, 0);
        ctx.lineTo(i * STEP_WIDTH + SIDE_WIDTH, height);
        ctx.closePath();
        ctx.stroke();
      }
    } else {
      for (let i = 0; i < 36; ++i) {
        ctx.fillStyle = blackKeys.includes(i) ? "black" : "white";
        ctx.fillRect(0, STEP_HEIGHT * i, SIDE_WIDTH, STEP_HEIGHT);
        if (i !== 0) {
          ctx.lineWidth = 0;
          ctx.strokeStyle = "#666";
          ctx.moveTo(0, STEP_HEIGHT * i + 0.5);
          ctx.lineTo(TOTAL_WIDTH, STEP_HEIGHT * i);
          ctx.stroke();
        }
      }
      for (let i = 0; i < 32; ++i) {
        ctx.moveTo(i * STEP_WIDTH + SIDE_WIDTH, 0);
        ctx.lineTo(i * STEP_WIDTH + SIDE_WIDTH, height);
        ctx.closePath();
        ctx.stroke();
      }
    }

    steps.forEach((step, stepIndex) => {
      step.forEach((note, noteIndex) => {
        if (note === true) {
          ctx.fillStyle = "#00ffcc";
          ctx.fillRect(
            stepIndex * STEP_WIDTH + SIDE_WIDTH + 2,
            noteIndex * STEP_HEIGHT + 2,
            STEP_WIDTH - 3,
            STEP_HEIGHT - 3
          );
        }
      });
    });
  }
};
