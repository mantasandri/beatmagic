import React from "react";
import Editor from "./Editor";

export default React.memo(function Track({
  type,
  index,
  muted,
  deleteTrack,
  gain,
  trackRef,
  toggleMute,
  setGain,
}) {
  function toggleMuted(e) {
    toggleMute(index, !muted);
  }

  function onGainChange(e) {
    setGain(index, e.target.value);
  }

  function deleteState() {
    deleteTrack(index);
  }

  return (
    <div className="track">
      <div className="track-info">
        <div className="track-title-wrapper">
          <button
            className={`track-number ${muted && "muted"}`}
            onClick={toggleMuted}
          >
            {index}
          </button>
          <h2 className="track-title">{type}</h2>
        </div>
        <label>
          Volumne
          <input
            type="range"
            className="gain"
            min="-20"
            max="5"
            step="0.5"
            defaultValue="0"
            onChange={onGainChange}
          />
        </label>
        <button className="track-delete" onClick={deleteState}>
          Remove
        </button>
      </div>
      <div className="track=steps">
        <Editor trackRef={trackRef} type={type} index={index} />
      </div>
    </div>
  );
});
