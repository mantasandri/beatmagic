import React, {useEffect, useState} from "react";
import * as Tone from "tone";
import Track from "./Track";
import {createDrumSteps, createNoteSteps, useForceUpdate} from "./util";

let drums = new Tone.Sampler({
  c1: "./audio/drums/kick.wav",
  "c#1": "./audio/drums/snare.wav",
  d1: "./audio/drums/hat.wav",
  "d#1": "./audio/drums/ohh.wav",
  f1: "./audio/drums/clap.wav",
  "f#1": "./audio/drums/rim.wav",
  g1: "./audio/drums/crash.wav",
  "g#1": "./audio/drums/ltom.wav",
  c2: "./audio/drums/htom.wav",
  "c#2": "./audio/drums/mtom1.wav",
  d2: "./audio/drums/mtom2.wav",
  "d#2": "./audio/drums/ftom.wav",
}).toMaster();


let elecBass = new Tone.Sampler({
  "A#2": "./audio/bass-electric/As2.[mp3|ogg]",
  "A#3": "./audio/bass-electric/As3.[mp3|ogg]",
  "A#4": "./audio/bass-electric/As4.[mp3|ogg]",
  "A#5": "./audio/bass-electric/As5.[mp3|ogg]",
  "C#2": "./audio/bass-electric/Cs2.[mp3|ogg]",
  "C#3": "./audio/bass-electric/Cs3.[mp3|ogg]",
  "C#4": "./audio/bass-electric/Cs4.[mp3|ogg]",
  "C#5": "./audio/bass-electric/Cs5.[mp3|ogg]",
  E2: "./audio/bass-electric/E2.[mp3|ogg]",
  E3: "./audio/bass-electric/E3.[mp3|ogg]",
  E4: "./audio/bass-electric/E4.[mp3|ogg]",
  E5: "./audio/bass-electric/E5.[mp3|ogg]",
  G2: "./audio/bass-electric/G2.[mp3|ogg]",
  G3: "./audio/bass-electric/G3.[mp3|ogg]",
  G4: "./audio/bass-electric/G4.[mp3|ogg]",
  G5: "./audio/bass-electric/G5.[mp3|ogg]",
}).toMaster();

let acoustic = new Tone.Sampler({
  F3: "./audio/guitar-acoustic/F3.[mp3|ogg]",
  "F#1": "./audio/guitar-acoustic/Fs1.[mp3|ogg]",
  "F#2": "./audio/guitar-acoustic/Fs2.[mp3|ogg]",
  "F#3": "./audio/guitar-acoustic/Fs3.[mp3|ogg]",
  G1: "./audio/guitar-acoustic/G1.[mp3|ogg]",
  G2: "./audio/guitar-acoustic/G2.[mp3|ogg]",
  G3: "./audio/guitar-acoustic/G3.[mp3|ogg]",
  "G#1": "./audio/guitar-acoustic/Gs1.[mp3|ogg]",
  "G#2": "./audio/guitar-acoustic/Gs2.[mp3|ogg]",
  "G#3": "./audio/guitar-acoustic/Gs3.[mp3|ogg]",
  A1: "./audio/guitar-acoustic/A1.[mp3|ogg]",
  A2: "./audio/guitar-acoustic/A2.[mp3|ogg]",
  A3: "./audio/guitar-acoustic/A3.[mp3|ogg]",
  "A#1": "./audio/guitar-acoustic/As1.[mp3|ogg]",
  "A#2": "./audio/guitar-acoustic/As2.[mp3|ogg]",
  "A#3": "./audio/guitar-acoustic/As3.[mp3|ogg]",
  B1: "./audio/guitar-acoustic/B1.[mp3|ogg]",
  B2: "./audio/guitar-acoustic/B2.[mp3|ogg]",
  B3: "./audio/guitar-acoustic/B3.[mp3|ogg]",
  C2: "./audio/guitar-acoustic/C2.[mp3|ogg]",
  C3: "./audio/guitar-acoustic/C3.[mp3|ogg]",
  C4: "./audio/guitar-acoustic/C4.[mp3|ogg]",
  "C#2": "./audio/guitar-acoustic/Cs2.[mp3|ogg]",
  "C#3": "./audio/guitar-acoustic/Cs3.[mp3|ogg]",
  "C#4": "./audio/guitar-acoustic/Cs4.[mp3|ogg]",
  D1: "./audio/guitar-acoustic/D1.[mp3|ogg]",
  D2: "./audio/guitar-acoustic/D2.[mp3|ogg]",
  D3: "./audio/guitar-acoustic/D3.[mp3|ogg]",
  D4: "./audio/guitar-acoustic/D4.[mp3|ogg]",
  "D#1": "./audio/guitar-acoustic/Ds1.[mp3|ogg]",
  "D#2": "./audio/guitar-acoustic/Ds2.[mp3|ogg]",
  "D#3": "./audio/guitar-acoustic/Ds3.[mp3|ogg]",
  E1: "./audio/guitar-acoustic/E1.[mp3|ogg]",
  E2: "./audio/guitar-acoustic/E2.[mp3|ogg]",
  E3: "./audio/guitar-acoustic/E3.[mp3|ogg]",
  F1: "./audio/guitar-acoustic/F1.[mp3|ogg]",
  F2: "./audio/guitar-acoustic/F2.[mp3|ogg]",
}).toMaster();

let electric = new Tone.Sampler({
  "D#3": "./audio/guitar-electric/Ds3.[mp3|ogg]",
  "D#4": "./audio/guitar-electric/Ds4.[mp3|ogg]",
  "D#5": "./audio/guitar-electric/Ds5.[mp3|ogg]",
  E2: "./audio/guitar-electric/E2.[mp3|ogg]",
  "F#2": "./audio/guitar-electric/Fs2.[mp3|ogg]",
  "F#3": "./audio/guitar-electric/Fs3.[mp3|ogg]",
  "F#4": "./audio/guitar-electric/Fs4.[mp3|ogg]",
  "F#5": "./audio/guitar-electric/Fs5.[mp3|ogg]",
  A2: "./audio/guitar-electric/A2.[mp3|ogg]",
  A3: "./audio/guitar-electric/A3.[mp3|ogg]",
  A4: "./audio/guitar-electric/A4.[mp3|ogg]",
  A5: "./audio/guitar-electric/A5.[mp3|ogg]",
  C3: "./audio/guitar-electric/C3.[mp3|ogg]",
  C4: "./audio/guitar-electric/C4.[mp3|ogg]",
  C5: "./audio/guitar-electric/C5.[mp3|ogg]",
  C6: "./audio/guitar-electric/C6.[mp3|ogg]",
  "C#2": "./audio/guitar-electric/Cs2.[mp3|ogg]",
}).toMaster();

let nylon = new Tone.Sampler({
  "F#2": "./audio/guitar-nylon/Fs2.[mp3|ogg]",
  "F#3": "./audio/guitar-nylon/Fs3.[mp3|ogg]",
  "F#4": "./audio/guitar-nylon/Fs4.[mp3|ogg]",
  "F#5": "./audio/guitar-nylon/Fs5.[mp3|ogg]",
  G3: "./audio/guitar-nylon/G3.[mp3|ogg]",
  G5: "./audio/guitar-nylon/G3.[mp3|ogg]",
  "G#2": "./audio/guitar-nylon/Gs2.[mp3|ogg]",
  "G#4": "./audio/guitar-nylon/Gs4.[mp3|ogg]",
  "G#5": "./audio/guitar-nylon/Gs5.[mp3|ogg]",
  A2: "./audio/guitar-nylon/A2.[mp3|ogg]",
  A3: "./audio/guitar-nylon/A3.[mp3|ogg]",
  A4: "./audio/guitar-nylon/A4.[mp3|ogg]",
  A5: "./audio/guitar-nylon/A5.[mp3|ogg]",
  "A#5": "./audio/guitar-nylon/As5.[mp3|ogg]",
  B1: "./audio/guitar-nylon/B1.[mp3|ogg]",
  B2: "./audio/guitar-nylon/B2.[mp3|ogg]",
  B3: "./audio/guitar-nylon/B3.[mp3|ogg]",
  B4: "./audio/guitar-nylon/B4.[mp3|ogg]",
  "C#3": "./audio/guitar-nylon/Cs3.[mp3|ogg]",
  "C#4": "./audio/guitar-nylon/Cs4.[mp3|ogg]",
  "C#5": "./audio/guitar-nylon/Cs5.[mp3|ogg]",
  D2: "./audio/guitar-nylon/D2.[mp3|ogg]",
  D3: "./audio/guitar-nylon/D3.[mp3|ogg]",
  D5: "./audio/guitar-nylon/D5.[mp3|ogg]",
  "D#4": "./audio/guitar-nylon/Ds4.[mp3|ogg]",
  E2: "./audio/guitar-nylon/E2.[mp3|ogg]",
  E3: "./audio/guitar-nylon/E3.[mp3|ogg]",
  E4: "./audio/guitar-nylon/E4.[mp3|ogg]",
  E5: "./audio/guitar-nylon/E5.[mp3|ogg]",
}).toMaster();

let piano = new Tone.Sampler({
  A0: "./audio/piano/A0.[mp3|ogg]",
  A1: "./audio/piano/A1.[mp3|ogg]",
  A2: "./audio/piano/A2.[mp3|ogg]",
  A3: "./audio/piano/A3.[mp3|ogg]",
  A4: "./audio/piano/A4.[mp3|ogg]",
  A5: "./audio/piano/A5.[mp3|ogg]",
  A6: "./audio/piano/A6.[mp3|ogg]",
  "A#0": "./audio/piano/As0.[mp3|ogg]",
  "A#1": "./audio/piano/As1.[mp3|ogg]",
  "A#2": "./audio/piano/As2.[mp3|ogg]",
  "A#3": "./audio/piano/As3.[mp3|ogg]",
  "A#4": "./audio/piano/As4.[mp3|ogg]",
  "A#5": "./audio/piano/As5.[mp3|ogg]",
  "A#6": "./audio/piano/As6.[mp3|ogg]",
  B0: "./audio/piano/B0.[mp3|ogg]",
  B1: "./audio/piano/B1.[mp3|ogg]",
  B2: "./audio/piano/B2.[mp3|ogg]",
  B3: "./audio/piano/B3.[mp3|ogg]",
  B4: "./audio/piano/B4.[mp3|ogg]",
  B5: "./audio/piano/B5.[mp3|ogg]",
  B6: "./audio/piano/B6.[mp3|ogg]",
  C0: "./audio/piano/C0.[mp3|ogg]",
  C1: "./audio/piano/C1.[mp3|ogg]",
  C2: "./audio/piano/C2.[mp3|ogg]",
  C3: "./audio/piano/C3.[mp3|ogg]",
  C4: "./audio/piano/C4.[mp3|ogg]",
  C5: "./audio/piano/C5.[mp3|ogg]",
  C6: "./audio/piano/C6.[mp3|ogg]",
  C7: "./audio/piano/C7.[mp3|ogg]",
  "C#0": "./audio/piano/Cs0.[mp3|ogg]",
  "C#1": "./audio/piano/Cs1.[mp3|ogg]",
  "C#2": "./audio/piano/Cs2.[mp3|ogg]",
  "C#3": "./audio/piano/Cs3.[mp3|ogg]",
  "C#4": "./audio/piano/Cs4.[mp3|ogg]",
  "C#5": "./audio/piano/Cs5.[mp3|ogg]",
  "C#6": "./audio/piano/Cs6.[mp3|ogg]",
  D0: "./audio/piano/D0.[mp3|ogg]",
  D1: "./audio/piano/D1.[mp3|ogg]",
  D2: "./audio/piano/D2.[mp3|ogg]",
  D3: "./audio/piano/D3.[mp3|ogg]",
  D4: "./audio/piano/D4.[mp3|ogg]",
  D5: "./audio/piano/D5.[mp3|ogg]",
  D6: "./audio/piano/D6.[mp3|ogg]",
  "D#0": "./audio/piano/Ds0.[mp3|ogg]",
  "D#1": "./audio/piano/Ds1.[mp3|ogg]",
  "D#2": "./audio/piano/Ds2.[mp3|ogg]",
  "D#3": "./audio/piano/Ds3.[mp3|ogg]",
  "D#4": "./audio/piano/Ds4.[mp3|ogg]",
  "D#5": "./audio/piano/Ds5.[mp3|ogg]",
  "D#6": "./audio/piano/Ds6.[mp3|ogg]",
  E0: "./audio/piano/E0.[mp3|ogg]",
  E1: "./audio/piano/E1.[mp3|ogg]",
  E2: "./audio/piano/E2.[mp3|ogg]",
  E3: "./audio/piano/E3.[mp3|ogg]",
  E4: "./audio/piano/E4.[mp3|ogg]",
  E5: "./audio/piano/E5.[mp3|ogg]",
  E6: "./audio/piano/E6.[mp3|ogg]",
  F0: "./audio/piano/F0.[mp3|ogg]",
  F1: "./audio/piano/F1.[mp3|ogg]",
  F2: "./audio/piano/F2.[mp3|ogg]",
  F3: "./audio/piano/F3.[mp3|ogg]",
  F4: "./audio/piano/F4.[mp3|ogg]",
  F5: "./audio/piano/F5.[mp3|ogg]",
  F6: "./audio/piano/F6.[mp3|ogg]",
  "F#0": "./audio/piano/Fs0.[mp3|ogg]",
  "F#1": "./audio/piano/Fs1.[mp3|ogg]",
  "F#2": "./audio/piano/Fs2.[mp3|ogg]",
  "F#3": "./audio/piano/Fs3.[mp3|ogg]",
  "F#4": "./audio/piano/Fs4.[mp3|ogg]",
  "F#5": "./audio/piano/Fs5.[mp3|ogg]",
  "F#6": "./audio/piano/Fs6.[mp3|ogg]",
  G0: "./audio/piano/G0.[mp3|ogg]",
  G1: "./audio/piano/G1.[mp3|ogg]",
  G2: "./audio/piano/G2.[mp3|ogg]",
  G3: "./audio/piano/G3.[mp3|ogg]",
  G4: "./audio/piano/G4.[mp3|ogg]",
  G5: "./audio/piano/G5.[mp3|ogg]",
  G6: "./audio/piano/G6.[mp3|ogg]",
  "G#0": "./audio/piano/Gs0.[mp3|ogg]",
  "G#1": "./audio/piano/Gs1.[mp3|ogg]",
  "G#2": "./audio/piano/Gs2.[mp3|ogg]",
  "G#3": "./audio/piano/Gs3.[mp3|ogg]",
  "G#4": "./audio/piano/Gs4.[mp3|ogg]",
  "G#5": "./audio/piano/Gs5.[mp3|ogg]",
  "G#6": "./audio/piano/Gs6.[mp3|ogg]",
}).toMaster();

let bass = new Tone.Sampler({
  f3: "./audio/808.wav",
}).toMaster();

let bell = new Tone.Sampler({
  c3: "./audio/bell.wav",
}).toMaster();

let drumMap = ["c1", "c#1", "d1", "d#1", "f1", "f#1", "g1", "g#1", "c2", "c#2", "d2", "d#2"];
let pianoMap = [
  "c2",
  "c#2",
  "d2",
  "d#2",
  "e2",
  "f2",
  "f#2",
  "g2",
  "g#2",
  "a2",
  "a#2",
  "b2",
  "c3",
  "c#3",
  "d3",
  "d#3",
  "e3",
  "f3",
  "f#3",
  "g3",
  "g#3",
  "a3",
  "a#3",
  "b3",
  "c4",
  "c#4",
  "d4",
  "d#4",
  "e4",
  "f4",
  "f#4",
  "g4",
  "g#4",
  "a4",
  "a#4",
  "b4",
];
let noteMap = [
  "e4",
  "d#4",
  "d4",
  "c#4",
  "c4",
  "b3",
  "a#3",
  "a3",
  "g#3",
  "g3",
  "f#3",
  "f3",
  "e3",
  "d#3",
  "d3",
  "c#3",
  "c3",
  "b2",
  "a#2",
  "a2",
  "g#2",
  "g2",
  "f#2",
  "f2",
];

const Header = React.memo(function ({
  bpm,
  updateBpm,
  playing,
  recording,
  togglePlay,
  toggleRecord,
}) {
  return (
    <div className="header">
      <h1>beatmagic</h1>
      <p>Inspired by Ken Wheeler | Built with React &amp; Tone.js</p>
      <div className="slider-container">
        BPM/Tempo:
        <input
          className="bpm"
          type="number"
          value={bpm}
          onChange={(e) => updateBpm(e.target.value)}
        />
        <input
          type="range"
          className="slider"
          value={bpm}
          min="30"
          max="300"
          onChange={(e) => updateBpm(e.target.value)}
        />
      </div>
      <button
        className="play-button"
        type="button"
        onClick={() => togglePlay()}
      >
        {playing ? "Stop" : "Play"}
      </button>
      {/* <button className={recording === true ? "record-active" : "record"} type="button" onClick={() => toggleRecord()}>
        {recording ? "STOP" : "RECORD"} (Not working ATM)
      </button> */}
      <br />
    </div>
  );
});

const AddTrack = React.memo(function ({type, onClick}) {
  return (
    <button onClick={() => onClick(type)} className="addTrack">
      Add {`${type}`} Track
    </button>
  );
});

export default function App() {
  let forceUpdate = useForceUpdate();
  let [bpm, updateBpm] = useState(120);
  let [playing, setPlaying] = useState(false);
  let [recording, setRecording] = useState(false);
  let [openSelect, setOpenSelect] = useState(false);
  let trackRef = React.useRef([]);
  let markerRef = React.useRef();

  function togglePlay() {
    if (Tone.Transport.state === "stopped") {
      Tone.Transport.start();
      markerRef.current.style.display = "block";
      setPlaying(true);
    } else {
      markerRef.current.style.display = "none";
      Tone.Transport.stop();
      setPlaying(false);
    }
  }

  function toggleRecord() {
    recording = !recording;
    if (recording) {
      setRecording(true);
      console.log("TRUE");
    } else {
      setRecording(false);
      console.log("FALSE");
    }
  }

  function toggleSelect() {
    openSelect = !openSelect;
    if (openSelect) {
      setOpenSelect(true);
    } else {
      setOpenSelect(false);
    }
    //
  }

  useEffect(() => {
    Tone.Transport.cancel();
    Tone.Transport.swingSubdivision = "16n";
    let loop = new Tone.Sequence(
      function (time, col) {
        trackRef.current.forEach((track) => {
          if (track.type === "drum") {
            let step = track.steps[col];
            step.forEach((note, noteIndex) => {
              if (note === true && track.muted === false) {
                drums.set("volume", track.gain);
                drums.triggerAttack(drumMap[noteIndex]);
              }
            });
          } else if (track.type === "bass") {
            let step = track.steps[col];
            step.forEach((note, noteIndex) => {
              if (note === true && track.muted === false) {
                bass.set("volume", track.gain);
                bass.triggerAttack(noteMap[noteIndex]);
              }
            });
          } else if (track.type === "elecBass") {
            let step = track.steps[col];
            step.forEach((note, noteIndex) => {
              if (note === true && track.muted === false) {
                elecBass.set("volume", track.gain);
                elecBass.triggerAttack(pianoMap[noteIndex]);
              }
            });
          } else if (track.type === "acoustic") {
            let step = track.steps[col];
            step.forEach((note, noteIndex) => {
              if (note === true && track.muted === false) {
                acoustic.set("volume", track.gain);
                acoustic.triggerAttack(pianoMap[noteIndex]);
              }
            });
          } else if (track.type === "electric") {
            let step = track.steps[col];
            step.forEach((note, noteIndex) => {
              if (note === true && track.muted === false) {
                electric.set("volume", track.gain);
                electric.triggerAttack(pianoMap[noteIndex]);
              }
            });
          } else if (track.type === "nylon") {
            let step = track.steps[col];
            step.forEach((note, noteIndex) => {
              if (note === true && track.muted === false) {
                nylon.set("volume", track.gain);
                nylon.triggerAttack(pianoMap[noteIndex]);
              }
            });
          } else if (track.type === "piano") {
            let step = track.steps[col];
            step.forEach((note, noteIndex) => {
              if (note === true && track.muted === false) {
                piano.set("volume", track.gain);
                piano.triggerAttack(pianoMap[noteIndex]);
              }
            });
          } else {
            let step = track.steps[col];
            step.forEach((note, noteIndex) => {
              if (note === true && track.muted === false) {
                bell.set("volume", track.gain);
                bell.triggerAttack(noteMap[noteIndex]);
              }
            });
          }
        });

        Tone.Draw.schedule(function () {
          markerRef.current.style.transform = `translateX(${
            col * 20.5 + 144
          }px)`;
        }, time);
      },
      [
        0,
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10,
        11,
        12,
        13,
        14,
        15,
        16,
        17,
        18,
        19,
        20,
        21,
        22,
        23,
        24,
        25,
        26,
        27,
        28,
        29,
        30,
        31,
      ],
      "16n"
    ).start(0);

    return () => {
      loop.dispose();
      loop = undefined;
    };
  }, [trackRef]);

  useEffect(() => {
    Tone.Transport.bpm.value = bpm;
  }, [bpm]);

  const updateTracks = (cb, update) => {
    cb(trackRef.current);
    if (update) {
      forceUpdate();
    }
  };

  function handleNewTrack(type) {
    updateTracks((state) => {
      state.push({
        type,
        gain: 0.5,
        muted: false,
        steps: type === "drum" ? createDrumSteps() : createNoteSteps(),
      });
    }, true);
    toggleSelect();
  }

  const toggleMute = (index, muted) => {
    updateTracks((state) => {
      state[index].muted = muted;
    }, true);
  };

  const deleteTrack = (index) => {
    updateTracks(() => {
      trackRef.current.splice(index, 1);
    }, true);
  };

  const setGain = (index, gain) => {
    updateTracks((state) => {
      state[index].gain = gain;
    });
  };

  const Dropdown = React.memo(function ({type, onClick, openSelect}) {
    return (
      <div className="selection-container">
        <button
          className="selector"
          type="button"
          onClick={() => onClick(type)}
        >
          {openSelect ? "Hide" : "Add Track"}
        </button>
        {openSelect && (
          <div className="addButtons">
            <AddTrack type="drum" onClick={handleNewTrack} />
            <AddTrack type="elecBass" onClick={handleNewTrack} />
            <AddTrack type="acoustic" onClick={handleNewTrack} />
            <AddTrack type="electric" onClick={handleNewTrack} />
            <AddTrack type="nylon" onClick={handleNewTrack} />
            <AddTrack type="piano" onClick={handleNewTrack} />
            {/* <AddTrack type="bass" onClick={handleNewTrack} /> */}
            {/* <AddTrack type="bell" onClick={handleNewTrack} /> */}
          </div>
        )}
      </div>
    );
  });

  return (
    <div className="beatmaker">
      <Header
        bpm={bpm}
        updateBpm={updateBpm}
        playing={playing}
        openSelect={openSelect}
        recording={recording}
        togglePlay={togglePlay}
        toggleRecord={toggleRecord}
        toggleSelect={toggleSelect}
      />
      <Dropdown onClick={() => toggleSelect()} openSelect={openSelect} />
      <div className="trackArea">
        <div ref={markerRef} className="marker" />
        {trackRef.current.map((t, i) => (
          <Track
            muted={t.muted}
            gain={t.gain}
            type={t.type}
            trackRef={trackRef}
            index={i}
            key={i + t.type}
            toggleMute={toggleMute}
            setGain={setGain}
            deleteTrack={deleteTrack}
          />
        ))}
      </div>
    </div>
  );
}
